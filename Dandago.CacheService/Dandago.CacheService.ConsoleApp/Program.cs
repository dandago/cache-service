﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.CacheService.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Server";

            var defaultMemoryCache = new DefaultMemoryCache();
            var cacheServer = new CacheServer(defaultMemoryCache, 6969);
            cacheServer.Start();

            Console.ReadLine();
        }
    }
}
