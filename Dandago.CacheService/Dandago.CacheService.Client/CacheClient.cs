﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

using Dandago.CacheService.Common;
using Newtonsoft.Json;
using System.Collections.Concurrent;

namespace Dandago.CacheService.Client
{
    public class CacheClient : CacheClientBase, IDisposable
    {
        private bool disposed = false;
        private object sendLock = new object();

        private string Host { get; }
        private int Port { get; }
        private StringBuilder Sb { get; }
        private ConcurrentDictionary<Guid, TaskCompletionSource<Response>> responseTasks;


        public CacheClient(string host, int port)
            : base()
        {
            this.Host = host;
            this.Port = port;

            this.responseTasks = new ConcurrentDictionary<Guid, TaskCompletionSource<Response>>();
        }

        public Task ConnectAsync()
        {
            var connectTcs = new TaskCompletionSource<bool>();
            this.TcpClient = new TcpClient();
            this.TcpClient.BeginConnect(this.Host, this.Port, HandleConnect, connectTcs);

            return connectTcs.Task;
        }

        public async Task<Response> GetAsync(string key)
        {
            var request = new Request();
            request.Get = true;
            request.Key = key;

            var response = await this.SendAsync(request);
            return response;
        }

        public async Task<Response> SetAsync(string key, string value, DateTimeOffset? expiry = null)
        {
            var request = new Request();
            request.Set = true;
            request.Key = key;
            request.Value = value;
            request.Expiry = expiry;

            var response = await this.SendAsync(request);
            return response;
        }

        public async Task<Response> DeleteAsync(string key)
        {
            var request = new Request();
            request.Delete = true;
            request.Key = key;

            var response = await this.SendAsync(request);
            return response;
        }

        public async Task<Response> DeleteGetAsync(string key)
        {
            var request = new Request();
            request.Get = true;
            request.Delete = true;
            request.Key = key;

            var response = await this.SendAsync(request);
            return response;
        }

        protected override void ProcessDataReceived(string guidStr, string jsonCommand)
        {
            // deserialize response

            var response = JsonConvert.DeserializeObject<Response>(jsonCommand);

            // handle command

            TaskCompletionSource<Response> tcs;
            var guid = Guid.Parse(guidStr);
            bool guidFound = this.responseTasks.TryGetValue(guid, out tcs);

            if (guidFound && tcs != null)
                tcs.SetResult(response);
        }

        private void HandleConnect(IAsyncResult asyncResult)
        {
            var connectTcs = asyncResult.AsyncState as TaskCompletionSource<bool>;

            this.TcpClient.EndConnect(asyncResult);

            this.StartReceiving();

            connectTcs.SetResult(true);
        }

        private Task<Response> SendAsync(Request request)
        {
            var guid = Guid.NewGuid();

            var jsonRequest = JsonConvert.SerializeObject(request);
            var tcs = new TaskCompletionSource<Response>();
            this.responseTasks.TryAdd(guid, tcs);

            var requestBytes = Encoding.UTF8.GetBytes(jsonRequest);
            string guidStr = guid.ToString("n");
            var guidBytes = Encoding.UTF8.GetBytes(guidStr);
            var expectData = ("EXPECT " + jsonRequest.Length).PadRight(ExpectCommandLength);
            var expectBytes = Encoding.UTF8.GetBytes(expectData);

            lock(sendLock) // prevent interleaving of message parts from different threads
            {
                this.Stream.Write(expectBytes, 0, expectBytes.Length);
                this.Stream.Write(guidBytes, 0, guidBytes.Length);
                this.Stream.Write(requestBytes, 0, requestBytes.Length);
                this.Stream.Flush();
            }

            return tcs.Task;
        }

        #region IDisposable implementation

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (this.TcpClient != null)
                    {
                        this.TcpClient.Close();
                    }
                }

                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        #endregion IDisposable implementation
    }
}
