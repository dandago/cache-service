﻿using Dandago.CacheService.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.CacheService
{
    internal class ResponseFactory
    {
        public Response CreateOkResponse(string data)
        {
            var response = new Response()
            {
                ResponseCode = ResponseCode.Ok,
                Data = data
            };

            return response;
        }

        public Response CreateNoResponse(string errorMessage)
        {
            var response = new Response()
            {
                ResponseCode = ResponseCode.No,
                ErrorMessage = errorMessage
            };

            return response;
        }

        public Response CreateErrorResponse(string errorMessage)
        {
            var response = new Response()
            {
                ResponseCode = ResponseCode.Error,
                ErrorMessage = errorMessage
            };

            return response;
        }
    }
}
