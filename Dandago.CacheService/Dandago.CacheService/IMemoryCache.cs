﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.CacheService
{
    public interface IMemoryCache
    {
        void Set(string key, string value);
        void Set(string key, string value, DateTimeOffset? expiry);
        string Get(string key);
        string Delete(string key);
    }
}
