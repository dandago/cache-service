﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

using Dandago.CacheService.Common;
using Newtonsoft.Json;

namespace Dandago.CacheService
{
    internal class CacheClient : CacheClientBase
    {
        private IMemoryCache memCache;
        private ResponseFactory responseFactory;

        public CacheClient(TcpClient tcpClient, IMemoryCache memCache)
            : base()
        {
            this.TcpClient = tcpClient;
            this.responseFactory = new ResponseFactory();
            this.memCache = memCache;
        }

        private void Send(string guid, byte[] bytes)
        {
            var expectData = ("EXPECT " + bytes.Length).PadRight(ExpectCommandLength);
            var expectBytes = Encoding.UTF8.GetBytes(expectData);

            var guidBytes = Encoding.UTF8.GetBytes(guid);

            this.Stream.Write(expectBytes, 0, expectBytes.Length);
            this.Stream.Write(guidBytes, 0, guidBytes.Length);
            this.Stream.Write(bytes, 0, bytes.Length);
            this.Stream.Flush();
        }

        private void Send(string guid, Response response)
        {
            var responseJson = JsonConvert.SerializeObject(response);
            var bytes = Encoding.UTF8.GetBytes(responseJson);
            this.Send(guid, bytes);
        }

        private void SendNo(string guid, string errorMessage)
        {
            var response = this.responseFactory.CreateNoResponse(errorMessage);
            this.Send(guid, response);
        }

        private void SendError(string guid, string errorMessage)
        {
            var response = this.responseFactory.CreateErrorResponse(errorMessage);
            this.Send(guid, response);
        }

        private void SendOk(string guid, string data)
        {
            var response = this.responseFactory.CreateOkResponse(data);
            this.Send(guid, response);
        }

        protected override void ProcessDataReceived(string guidStr, string jsonCommand)
        {
            // deserialize JSON command

            var command = JsonConvert.DeserializeObject<Request>(jsonCommand);

            // handle command

            if (command.Set == true && !string.IsNullOrEmpty(command.Key)
                && !string.IsNullOrEmpty(command.Value))
            { // set command
                this.memCache.Set(command.Key, command.Value, command.Expiry);
                this.SendOk(guidStr, "Saved");
            }
            else if (command.Delete == true && !string.IsNullOrEmpty(command.Key))
            { // del and delget commands
                var data = this.memCache.Delete(command.Key);
                if (data == null)
                    this.SendNo(guidStr, "Not found");
                else
                    this.SendOk(guidStr, (command.Get == true ? data.ToString() : "Removed"));
            }
            else if (command.Get == true && !string.IsNullOrEmpty(command.Key))
            { // get command
                var data = this.memCache.Get(command.Key);
                if (data == null)
                    this.SendNo(guidStr, "Not found");
                else
                    this.SendOk(guidStr, data.ToString());
            }
            else
                this.SendError(guidStr, "Unrecognized command.");
        }
    }
}
