﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.CacheService
{
    public class DefaultMemoryCache : IMemoryCache
    {
        private MemoryCache memCache;

        public DefaultMemoryCache()
        {
            this.memCache = MemoryCache.Default;
        }

        public void Set(string key, string value)
        {
            this.memCache.Set(key, value, null);
        }

        public void Set(string key, string value, DateTimeOffset? expiry)
        {
            if (expiry.HasValue)
                this.memCache.Set(key, value, expiry.Value);
            else
                this.memCache.Set(key, value, null);
        }

        public string Get(string key)
        {
            return this.memCache.Get(key) as string;
        }

        public string Delete(string key)
        {
            return this.memCache.Remove(key) as string;
        }
    }
}
