﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.CacheService
{
    public class CacheServer
    {
        private int port;

        public IMemoryCache MemCache { get; }
        public TcpListener Server { get; }

        public CacheServer(IMemoryCache memCache, int port)
        {
            this.MemCache = memCache;
            this.port = port;
        }

        public void Start()
        {
            var server = new TcpListener(IPAddress.Any, this.port);
            server.Start();

            server.BeginAcceptTcpClient(HandleAcceptTcpClient, server);
        }

        private void HandleAcceptTcpClient(IAsyncResult asyncResult)
        {
            var server = asyncResult.AsyncState as TcpListener;
            var tcpClient = server.EndAcceptTcpClient(asyncResult);
            var cacheClient = new CacheClient(tcpClient, this.MemCache);
            cacheClient.StartReceiving();

            server.BeginAcceptTcpClient(HandleAcceptTcpClient, server);
        }
    }
}
