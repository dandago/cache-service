﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.CacheService.Common
{
    public class Request
    {
        // required stuff
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Key { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public DateTimeOffset? Expiry { get; set; }

        // operations
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? Get { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? Delete { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public bool? Set { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();

            if (this.Get == true)
                sb.Append("GET");
            if (this.Delete == true)
                sb.Append("DEL");
            if (this.Set == true)
                sb.Append("SET");

            sb.Append(" ");

            sb.Append(this.Key);

            if (!string.IsNullOrEmpty(this.Value))
                sb.AppendFormat(" {0}", this.Value);

            return sb.ToString();
        }
    }
}
