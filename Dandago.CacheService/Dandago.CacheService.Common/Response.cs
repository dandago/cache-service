﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.CacheService.Common
{
    public class Response
    {
        public ResponseCode ResponseCode { get; set; }
        public string ErrorMessage { get; set; }
        public string Data { get; set; }

        public override string ToString()
        {
            return $"{this.ResponseCode} {(this.Data == null ? this.ErrorMessage : this.Data)}";
        }
    }
}
