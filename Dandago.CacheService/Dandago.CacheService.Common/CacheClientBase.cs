﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Dandago.CacheService.Common
{
    public abstract class CacheClientBase
    {
        private static int initialBufferSize = 16;
        protected static int ExpectCommandLength = 16;
        private static int maxBufferSize = 4 * 1024 * 1024; // 4MB max
        private static int guidLength = 32;
        private int expectedBytes;
        private int receivedBytes;

        protected TcpClient TcpClient { get; set; }
        public byte[] Buffer { get; private set; }
        private StringBuilder Sb { get; }

        protected Stream Stream
        {
            get
            {
                return this.TcpClient?.GetStream();
            }
        }

        public CacheClientBase()
        {
            this.Buffer = new byte[initialBufferSize];
            this.Sb = new StringBuilder();
        }

        protected abstract void ProcessDataReceived(string guidStr, string jsonCommand);

        public void StartReceiving()
        {
            this.Stream.BeginRead(this.Buffer, 0, this.Buffer.Length,
                HandleDataReceived, this.Stream);
        }

        protected void HandleDataReceived(IAsyncResult asyncResult)
        {
            int bytesRead = this.Stream.EndRead(asyncResult);

            for (int i = 0; i < bytesRead; i++)
                this.Sb.Append((char)this.Buffer[i]);

            this.receivedBytes += bytesRead;

            if (this.expectedBytes == 0 && this.receivedBytes >= ExpectCommandLength)
            { // we didn't get an expected byte count, and can read it now
                var expectBuffer = new char[ExpectCommandLength];
                this.Sb.CopyTo(0, expectBuffer, 0, ExpectCommandLength);
                var expectCommand = new string(expectBuffer);

                if (expectCommand.StartsWith("expect", StringComparison.InvariantCultureIgnoreCase))
                {
                    string expectValueStr = expectCommand.Substring(7);
                    bool converted = int.TryParse(expectValueStr, out this.expectedBytes);

                    if (converted)
                    {
                        // set buffer with the expected size (or max)
                        // so we get a large bufferfuls of data at a time

                        int newBufferSize = Math.Min(this.expectedBytes, maxBufferSize);
                        newBufferSize = Math.Max(newBufferSize, initialBufferSize);
                        this.Buffer = new byte[newBufferSize];

                        // remove the expect command from the beginning of
                        // the buffer, since we don't need it any more

                        this.Sb.Remove(0, ExpectCommandLength);
                    }
                    else
                    {
                        // can't parse expect; ignore
                    }
                }
                else
                {
                    // invalid command where EXPECT was expected; ignore
                }
            }
            else if (this.receivedBytes == this.expectedBytes + ExpectCommandLength + guidLength)
            { // we got all the data we were expecting; reset everything
                this.expectedBytes = 0;
                this.receivedBytes = 0;
                this.Buffer = new byte[initialBufferSize];

                // get the guid first

                var requestGuidStr = this.Sb.ToString(0, 32);

                // get the command from sb buffer

                var jsonCommand = this.Sb.ToString(32, this.Sb.Length - 32);
                this.Sb.Clear();

                // process command

                this.ProcessDataReceived(requestGuidStr, jsonCommand);
            }

            this.StartReceiving();
        }
    }
}
