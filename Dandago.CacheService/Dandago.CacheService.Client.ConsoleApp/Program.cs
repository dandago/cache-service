﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dandago.CacheService.Client.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Client";

            Thread.Sleep(3000);

            Task.Run(async () =>
            {
                var client = new CacheClient("localhost", 6969);
                Console.Write("Connecting... ");
                await client.ConnectAsync();
                Console.WriteLine("Done");

                // Test 1: SET, GET, GET after expiry

                Console.WriteLine("-- TEST 1 --");

                Console.WriteLine("Sending SET");
                var expiry = new DateTimeOffset(DateTime.Now);
                expiry = expiry.AddSeconds(2.0);
                var response1 = await client.SetAsync("Test", "1", expiry);
                Console.WriteLine(response1);

                Console.WriteLine("Sending GET");
                var response2 = await client.GetAsync("Test");
                Console.WriteLine(response2);

                Thread.Sleep(3000);

                Console.WriteLine("Sending GET");
                var response3 = await client.GetAsync("Test");
                Console.WriteLine(response3);

                // Test 2: SET, GET, DEL, GET

                Console.WriteLine("-- TEST 2 --");

                Console.WriteLine("Sending SET");
                var response4 = await client.SetAsync("Test2", "1");
                Console.WriteLine(response4);

                Console.WriteLine("Sending GET");
                var response5 = await client.GetAsync("Test2");
                Console.WriteLine(response5);

                Console.WriteLine("Sending DEL");
                var response6 = await client.DeleteAsync("Test2");
                Console.WriteLine(response6);

                Console.WriteLine("Sending GET");
                var response7 = await client.GetAsync("Test2");
                Console.WriteLine(response7);

                // Test 3: SET, DELGET, GET

                Console.WriteLine("-- TEST 3 --");

                Console.WriteLine("Sending SET");
                var response8 = await client.SetAsync("Test3", "1");
                Console.WriteLine(response8);

                Console.WriteLine("Sending DELGET");
                var response9 = await client.DeleteGetAsync("Test3");
                Console.WriteLine(response9);

                Console.WriteLine("Sending GET");
                var response10 = await client.GetAsync("Test3");
                Console.WriteLine(response10);
            });

            Console.ReadLine();
        }
    }
}
